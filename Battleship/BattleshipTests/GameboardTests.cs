using Battleship;
using Battleship.Enumerators;
using Xunit;

namespace BattleshipTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(10,10,CellState.Empty)]
        [InlineData(20, 20,CellState.Empty)]
        public void GameboardTest(int rows, int columns,CellState cellState)
        {
           Gameboard gameboard = new Gameboard(rows, columns, cellState);

           Assert.Equal(rows * columns, gameboard.Size);

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    Assert.Equal(CellState.Empty, gameboard.CellStates[r, c]);
                }
            }
        }

    }
}
