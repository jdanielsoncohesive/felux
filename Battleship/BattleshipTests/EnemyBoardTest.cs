﻿using System;
using Battleship;
using Battleship.Enumerators;
using Xunit;

namespace BattleshipTests
{
    public class EnemyBoardTest
    {
        [Theory]
        [InlineData(10, 10, CellState.Empty)]
        [InlineData(100, 100, CellState.Empty)]
        public void EnemyBoardCreationTest(int rows, int columns, CellState cellState)
        {
            EnemyBoard enemyBoard = new EnemyBoard(rows, columns, cellState);

            Assert.Equal(rows * columns, enemyBoard.Size);

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    Assert.Equal(CellState.Empty, enemyBoard.CellStates[r, c]);
                }
            }
        }

        [Fact]
        public void PlaceMarkerTest()
        {
            EnemyBoard enemyBoard = new EnemyBoard(10, 10);

            //Test Hit mark
            enemyBoard.PlaceMarker(4, 4, CellState.Hit);
            Assert.Equal(CellState.Hit, enemyBoard.CellStates[4, 4]);
            Assert.NotEqual(CellState.Miss, enemyBoard.CellStates[4, 4]);

            //Test Miss mark
            enemyBoard.PlaceMarker(9, 9, CellState.Miss);
            Assert.Equal(CellState.Miss, enemyBoard.CellStates[9, 9]);
            Assert.NotEqual(CellState.Hit, enemyBoard.CellStates[9, 9]);

            //Test setting marker in same spot
            Assert.Throws<Exception>(() => enemyBoard.PlaceMarker(4, 4, CellState.Miss));
            Assert.Throws<Exception>(() => enemyBoard.PlaceMarker(9, 9, CellState.Hit));

        }

    }
}
