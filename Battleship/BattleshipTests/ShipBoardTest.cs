﻿using System;
using Battleship;
using Battleship.Enumerators;
using Battleship.Models.Ships;
using Xunit;

namespace BattleshipTests
{
    public class PlacementTests
    {
        [Theory]
        [InlineData(10, 10, CellState.Empty)]
        [InlineData(100, 100, CellState.Empty)]
        public void ShipBoardCreationTest(int rows, int columns, CellState cellState)
        {
            ShipBoard shipBoard = new ShipBoard(rows, columns, cellState);

            Assert.Equal(rows * columns, shipBoard.Size);

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    Assert.Equal(CellState.Empty, shipBoard.CellStates[r, c]);
                }
            }
        }

        [Fact]
        public void TestPlaceHorizontalShips()
        {
            ShipBoard shipBoard = new ShipBoard(10, 10);

            shipBoard.PlaceShip(0, 0, new Carrier(ShipOrientation.Horizontal));
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[0,0]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[1, 0]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[2, 0]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[3, 0]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[4, 0]);

            //Try putting one in same place
            Assert.Throws<Exception>(() =>
                shipBoard.PlaceShip(0, 0, new Carrier(ShipOrientation.Horizontal)));

            Assert.Throws<Exception>(() =>
                shipBoard.PlaceShip(9, 9, new Submarine(ShipOrientation.Horizontal)));


        }

        [Fact]
        public void TestPlaceVerticalShips()
        {
            ShipBoard shipBoard = new ShipBoard(10, 10);

            shipBoard.PlaceShip(0, 0, new Carrier(ShipOrientation.Veritcal));
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[0, 0]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[0, 1]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[0, 2]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[0, 3]);
            Assert.Equal(CellState.Occupied, shipBoard.CellStates[0, 4]);

            //Try putting one in same place
            Assert.Throws<Exception>(() =>
                shipBoard.PlaceShip(0, 0, new Carrier(ShipOrientation.Veritcal)));

            Assert.Throws<Exception>(() =>
                shipBoard.PlaceShip(9, 9, new Submarine(ShipOrientation.Veritcal)));

        }
    }
}
