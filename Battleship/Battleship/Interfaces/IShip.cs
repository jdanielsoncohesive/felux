﻿using System;
using Battleship.Enumerators;

namespace Battleship.Interfaces
{
    public interface IShip
    { 
        public abstract ShipType ShipType { get;}
        public abstract ShipOrientation ShipOrientation { get; }
        public abstract int Size { get; }
        public abstract int HitCount { get; set; }
        public bool IsSunk { get; }
    }
}
