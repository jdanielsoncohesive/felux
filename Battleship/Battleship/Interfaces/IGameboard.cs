﻿using System;
using Battleship.Enumerators;

namespace Battleship.Interfaces
{
    public interface IGameboard
    {
       public int Size { get; }
       public int Rows { get; }
       public int Columns { get; }
       public CellState[,] CellStates { get; }
    }
}
