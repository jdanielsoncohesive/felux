﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship
{
    public class Gameboard : IGameboard
    {
        public int Size { get { return Rows * Columns; } }
        public int Rows { get; }
        public int Columns { get; }
        public CellState[,] CellStates { get; }

        public Gameboard(int rows, int columns, CellState defaultState = CellState.Empty)
        {
            Rows = rows;
            Columns = columns;
            CellStates = new CellState[Rows, Columns];
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Columns; c++)
                {
                    CellStates[r, c] = defaultState;
                }
            }
        }
     
    }
}
