﻿using System;
namespace Battleship.Enumerators
{
    public enum ShipType
    {
        Carrier,
        Battleship,
        Cruiser,
        Submarine,
        Destroyer
    }
}
