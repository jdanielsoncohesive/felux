﻿using System;
namespace Battleship.Enumerators
{
    public enum CellState
    {
       Occupied,
       Empty,
       Hit,
       Miss
    }
}
