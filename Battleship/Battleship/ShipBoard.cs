﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship
{
    public class ShipBoard : Gameboard
    {
        public ShipBoard(int rows, int columns, CellState cellState = CellState.Empty) : base
            (rows, columns, cellState)
        {

        }

        public bool PlaceShip(int row, int column, IShip ship) {
            // My assumption for this exercixe is that ui component could
            // be developed for visual purposes to see the grid later.
            // But this should be core placing logic. Potentially
            // a GameManager class or PlayerClass of sorts could
            // handle tracking ships a player has placed, and hits on ships
            // Here we loop over size and direction of ship before placing
            // if theyre all valid, then we'll loop through again
            // and update positions. If there is something there, throw
            // an error saying so

            if ((row+ship.Size) >= Rows || (column + ship.Size) >= Columns) {
                throw new Exception("Invalid Placement. Out of bounds");
            }
           
            for(int i = 0; i < ship.Size; i++) {
                if(ship.ShipOrientation == ShipOrientation.Veritcal) {
                    if (CellStates[row,column + i] != CellState.Empty) {
                        throw new Exception("Invalid Placement. Cell occupied.");
                    } 
                } else {
                    if (CellStates[row + i, column] != CellState.Empty)
                    {
                        throw new Exception("Invalid Placement. Cell occupied");
                    }
                }
            }

            // Made it here ... update status of cell to occupied
            for (int i = 0; i < ship.Size; i++) {
                if (ship.ShipOrientation == ShipOrientation.Veritcal) {
                    CellStates[row, column + i] = CellState.Occupied;
                }
                else {
                    CellStates[row + i, column] = CellState.Occupied;
                }
            }

            return true;
        }

    }
}
