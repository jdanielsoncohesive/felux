﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship.Models.Ships
{
    public class Cruiser : IShip
    {
        public int Size { get; }
        public ShipType ShipType { get; }
        public ShipOrientation ShipOrientation { get; }
        public int HitCount { get; set; }

        public Cruiser(ShipOrientation shipOrientation)
        {
            ShipType = ShipType.Cruiser;
            Size = 3;
            ShipOrientation = ShipOrientation;
        }

        public bool IsSunk { get { return HitCount >= Size; } }

    }
}
