﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship.Models.Ships
{
    public class Carrier : IShip
    {
        public int Size { get; }
        public ShipType ShipType { get; }
        public ShipOrientation ShipOrientation { get; }
        public int HitCount { get; set; }

        public Carrier(ShipOrientation shipOrientation)
        {
            ShipType = ShipType.Carrier;
            Size = 5;
            ShipOrientation = shipOrientation;
        }

        public bool IsSunk { get { return HitCount >= Size; } }

    }
}
