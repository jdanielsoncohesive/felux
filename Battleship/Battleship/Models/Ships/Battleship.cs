﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship.Models.Ships
{
    public class Battleship : IShip
    {
        public int Size { get; }
        public ShipType ShipType { get; }
        public ShipOrientation ShipOrientation { get; }
        public int HitCount {get; set;}

        public Battleship(ShipOrientation shipOrientation)
        {
            ShipType = ShipType.Battleship;
            Size = 4;
            ShipOrientation = ShipOrientation;
        }

         public bool IsSunk { get { return HitCount >= Size; } }
    }
}
