﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship.Models.Ships
{
    public class Destroyer : IShip
    {
        public int Size { get; }
        public ShipType ShipType { get; }
        public ShipOrientation ShipOrientation { get; }
        public int HitCount { get; set; }

        public Destroyer(ShipOrientation shipOrientation)
        {
            ShipType = ShipType.Destroyer;
            Size = 2;
            ShipOrientation = shipOrientation;
        }

        public bool IsSunk { get { return HitCount >= Size; } }
    }
}
