﻿using System;
using Battleship.Enumerators;
using Battleship.Interfaces;

namespace Battleship.Models.Ships
{
    public class Submarine : IShip
    {
        public int Size { get; }
        public ShipType ShipType { get; }
        public ShipOrientation ShipOrientation { get; }
        public int HitCount { get; set; }

        public Submarine(ShipOrientation shipOrientation)
        {
            ShipType = ShipType.Submarine;
            Size = 3;
            ShipOrientation = shipOrientation;
        }

        public bool IsSunk { get { return HitCount >= Size; } }
    }
}
