﻿using System;
using Battleship.Enumerators;

namespace Battleship
{
    public class EnemyBoard : Gameboard
    {
        public EnemyBoard(int rows, int columns, CellState cellState = CellState.Empty) : base
            (rows, columns, cellState)
        {

        }

        public bool PlaceMarker(int row, int column, CellState cellState)
        {
            // For this exercise, we're(I'm ?) assuming we yelled out
            // our fire position to a player who also has a digtal 
            // board system. When they tell us the result, we need to 
            // just log it. If there was a UI, we could call the cellState 
            // for this board instance and display it visually with a HIT,
            // MISS, EMPTY styling of sorts

            if (row >= Rows || column >= Columns)
            {
                throw new Exception("Invalid Position. Out of bounds");
            }

            if (CellStates[row, column] != CellState.Empty)
            {
                throw new Exception($"Invalid Position. Cell already marked: {CellStates[row,column]}");
            }

            this.CellStates[row, column] = cellState;


            return true;
        }
    }
}
